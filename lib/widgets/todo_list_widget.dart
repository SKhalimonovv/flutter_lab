import 'package:flutter/material.dart';

class TodoList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TodoListState();
  }
}

class TodoListState extends State<TodoList> {
  final TextEditingController _textFieldController = TextEditingController();
  final List<String> _items = ['Item 1', 'Item 2', 'Item 3'];

  Widget _buildList() {
  return ListView.builder(
    itemCount: _items.length,
    itemBuilder: (BuildContext ctxt, int index) {
      final String item = _items[index];
      return Dismissible(
        key: Key(item),
        onDismissed: (DismissDirection direction) {
          setState(() {
            _items.removeAt(index);
          });

          Scaffold.of(ctxt)
              .showSnackBar(SnackBar(content: Text('$item dismissed')));
        },
        background: Container(color: Colors.red),
        child: ListTile(title: Text('$item')),
      );
    });
  }

  void _addNewItem(BuildContext context) {
    showDialog<AlertDialog>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Write new action todo'),
          content: TextField(
            controller: _textFieldController,
            decoration: InputDecoration(hintText: 'Todo action'),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('CANCEL'),
              onPressed: () {
                Navigator.of(context).pop();
              },
              ),
              FlatButton(
                child: Text('Add'),
                onPressed: () {
                  setState(() {
                    if (_textFieldController.text.length > 0) {
                      _items.add(_textFieldController.text);
                      _textFieldController.clear();
                    }
                  });
                  Navigator.of(context).pop();
                },
              )
          ],
        );
      });
  }

  @override
  Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(title: Text("Todo list")),
    body: _buildList(),
    floatingActionButton: FloatingActionButton(
      onPressed: () => _addNewItem(context),
      tooltip: 'Add',
      child: Icon(Icons.add),
    )
  );
}
}